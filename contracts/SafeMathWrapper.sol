pragma solidity ^0.8.0;

// This file was generated by Solid-Contract.com software.

import "./SafeMath.sol";

contract SafeMathWrapper {

  function tryAdd(uint256 a, uint256 b) public pure returns (bool, uint256) {
    return SafeMath.tryAdd(a, b);
  }

  function trySub(uint256 a, uint256 b) public pure returns (bool, uint256) {
    return SafeMath.trySub(a, b);
  }

  function tryMul(uint256 a, uint256 b) public pure returns (bool, uint256) {
    return SafeMath.tryMul(a, b);
  }

  function tryDiv(uint256 a, uint256 b) public pure returns (bool, uint256) {
    return SafeMath.tryDiv(a, b);
  }

  function tryMod(uint256 a, uint256 b) public pure returns (bool, uint256) {
    return SafeMath.tryMod(a, b);
  }

  function add(uint256 a, uint256 b) public pure returns (uint256) {
    return SafeMath.add(a, b);
  }

  function sub(uint256 a, uint256 b) public pure returns (uint256) {
    return SafeMath.sub(a, b);
  }

  function mul(uint256 a, uint256 b) public pure returns (uint256) {
    return SafeMath.mul(a, b);
  }

  function div(uint256 a, uint256 b) public pure returns (uint256) {
    return SafeMath.div(a, b);
  }

  function mod(uint256 a, uint256 b) public pure returns (uint256) {
    return SafeMath.mod(a, b);
  }

  function sub(uint256 a, uint256 b, string memory errorMessage) public pure returns (uint256) {
    return SafeMath.sub(a, b, errorMessage);
  }

  function div(uint256 a, uint256 b, string memory errorMessage) public pure returns (uint256) {
    return SafeMath.div(a, b, errorMessage);
  }

  function mod(uint256 a, uint256 b, string memory errorMessage) public pure returns (uint256) {
    return SafeMath.mod(a, b, errorMessage);
  }
}
# Solid Contract Example

This is the result of the proof-of-concept software that generates tests for Solidity Contracts.
The interesting generated file is `tests/SafeMath_test.js`. It achieves 100% coverage for the underlying contract.

## How to run

- clone this repo to your local machine and cd into it.
- run `npm install`
- run `npx hardhat compile`
- To run the tests with coverage, run `npx hardhat coverage`